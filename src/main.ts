import './assets/main.css';


import { createApp, provide, h } from 'vue';
import { DefaultApolloClient } from '@vue/apollo-composable';
// Подключение graphql
import { ApolloClient, HttpLink, InMemoryCache } from '@apollo/client/core';

import { createPinia } from 'pinia';
import App from './App.vue';
import router from './router';



const pinia = createPinia();

// Cache implementation
const cache = new InMemoryCache();

const httpLink = new HttpLink({
  uri: 'http://localhost:8787/graphql',
});

// Create the apollo client
const apolloClient = new ApolloClient({
  link: httpLink,
  cache: cache,
  connectToDevTools: true,
});


const application = createApp({
  setup() {    
    provide(DefaultApolloClient, apolloClient);
  },

  render: () => h(App),
});

application.use(router)
application.use(pinia)
application.mount('#app')